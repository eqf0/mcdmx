import std/[tables, sets, sequtils]

type
  Station* = object
    name*, description*: string
    lines*: seq[string]
    coords*: seq[float]

  MetroLine* = object
    name*: string
    stations*: seq[string]

  MetroSystem* = object
    lines*: Table[string, MetroLine]
    stations*: Table[string, Station]

  Segment* = object
    line: string
    stations: seq[string]

  Route* = object
    currStation: Station
    segments: seq[Segment]

proc `$`*(s: Segment): string =
  if s.stations.len == 0:
    result = "empty segment..."
  else:
    result.add(s.line & "\n")
    result.add("\t" & s.stations[0] & " -> " & s.stations[^1] & "\n")
    result.add("\t" & $(s.stations.len - 1) & " stations\n")

iterator items*(r: Route): Segment =
  for s in r.segments:
    yield s

func initRoute*(): Route = Route(segments: newSeq[Segment]())

func initMetroLine*(s: string): MetroLine =
  MetroLine(name: s)

func initStation*(s: string, c: seq[float]): Station =
  Station(name: s, coords: c)

func `$`*(s: Station): string =
  s.name & ": " & $s.coords & " : " & $s.lines

func `$`*(l: MetroLine): string =
  result = l.name & "\n"
  for s in l.stations:
    result.add "\t" & $s & "\n"

proc add*(r: var Route, s: Station) =
  if r.segments.len == 0:
    r.segments.add Segment(
      line: s.lines[0],
      stations: @[s.name]
    )

  else:
    let
      currLines = r.currStation.lines.toHashSet()
      nextLines = s.lines.toHashSet()
      commonLines = currLines * nextLines
    let nextLine = commonLines.toSeq()[0]

    var currLine = r.segments[^1].line
    if nextLine == currLine:
      r.segments[^1].stations.add s.name
    else:
      r.segments.add Segment(
        line: nextLine,
        stations: @[r.currStation.name, s.name]
      )

  r.currStation = s

