import
  std/[xmltree, xmlparser, sugar, strutils, tables],
  ./metro

proc parseCoords(s: string): seq[float] =
  for l in s.strip().split(','):
    result.add(parseFloat(l))

proc scrapLines(node: XmlNode, stations: var Table[string, Station]): Table[string, MetroLine] =
  for n in node.findAll("Placemark"):
    let name = n.child("name").innerText().strip()
    var line = initMetroLine(name)

    let
      posStr = n.findAll("coordinates")[0].innerText
      coords = collect:
        for line in posStr.strip().split('\n'):
          line.strip().parseCoords()

    for c in coords:
      for s in stations.mvalues:
        if c == s.coords:
          line.stations.add s.name
          s.lines.add line.name

    result[name] = line

proc scrapStations(node: XmlNode): Table[string, Station] =
  for n in node.findAll("Placemark"):
    let
      name = n.child("name").innerText().strip()
      desc = n.child("description").innerText()
      coords = n.findAll("coordinates")[0].innerText().parseCoords()

    result[name] = initStation(name, coords)

proc scrapMetroSystem*(file: string): MetroSystem =
  let
    map = file.slurp().parseXml()
    places = map.findAll("Folder")

  result.stations = places[1].scrapStations()
  result.lines = places[0].scrapLines(result.stations)

