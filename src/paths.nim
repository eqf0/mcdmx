import
  std/[sets, deques, strtabs, options, tables],
  pkg/gram,
  ./metro

const metroFlags = {UniqueNodes, Undirected, ValueIndex}.toInt

proc bfs*(g: Graph, s, t: Graph.N): Option[seq[Graph.N]] =
  if s in g:
    var
      queue = [s].toDeque
      tree = newGraph[Graph.N, string]()

    discard tree.add s

    while 0 < queue.len:
      let n = queue.popLast()
      if n == t:
        break

      for _, node in g[n].neighbors:
        let m = node.value
        if m notin tree:
          let nnode = tree.add m
          discard tree.edge(nnode, "is child of", tree[n])
          queue.addFirst m

    if t in tree:
      var
        curr = tree[t]
        begin = tree[s]
        path = @[t]

      while curr != begin:
        curr = curr["is child of"]
        path.add(curr.value)

      result = some path

proc buildGraph*(metro: MetroSystem): auto =
  result = newGraph[string, float](wanted = metroFlags)

  for l in metro.lines.values:
    let st = l.stations
    for i in 0 ..< st.len:
      let station = st[i]
      if station notin result:
        discard result.add station
      if 0 < i:
        let prevStation = st[i-1]
        discard result.edge(result[prevStation], 0, result[station])
