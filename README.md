# Mexico City Metro System

Following [this](https://gist.github.com/MachinesAreUs/a28c0173fd0d4a57d534129d9d6bcafb) challenge.

## Usage

Build

```shell
nimble build
```

and run

```shell
/.mcdmx <source> <target>
```

where the arguments are the name of some Mexico City metro stations

## Example

```shell
./mcdmx Vallejo Iztapalapa
Línea 8
        Iztapalapa -> Santa Anita
        7 stations

Línea 4
        Santa Anita -> Consulado
        6 stations

Línea 5
        Consulado -> Instituto del Petróleo
        5 stations

Línea 6
        Instituto del Petróleo -> Vallejo
        1 stations
```
