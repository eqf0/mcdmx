# Package

version       = "0.1.0"
author        = "eqf0"
description   = "Mexico City Metro Systems map"
license       = "MIT"
srcDir        = "src"
bin           = @["mcdmx"]


# Dependencies

requires "nim >= 1.6.4"
requires "https://github.com/disruptek/gram"

task cleanExecs, "remove executables":
  for file in thisDir().listFiles():
    if file.split('.').len() == 1:
      rmFile(file)

  for dir in thisDir().listDirs():
    for file in dir.listFiles:
      if file.split('.').len() == 1:
        rmFile(file)

task clean, "deletes generated files":
  rmDir("testresults")
  rmDir("htmldocs")
  rmDir("nimcache")
  cleanExecsTask()

task buildExamples, "build examples":
  for f in "examples".listFiles():
    exec "nim c " & f
